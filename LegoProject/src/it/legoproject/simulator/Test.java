package it.legoproject.simulator;

import it.legoproject.Utils;
import it.legoproject.simulator.FakeRobot.State;
import it.legoproject.simulator.sensor.ColorSensor;

import org.junit.Before;

public class Test {

	private FakeRobot r;
	private ColorSensor c;
	
	@Before
	public void build() {
		r = new FakeRobot();
		c = new ColorSensor(Utils.COLOR_SENSOR);
		c.setObserver(r);
	}
	
	@org.junit.Test
	public void test1() {
		assert(r.state == State.idle);
	}
	
	@org.junit.Test
	public void test2() {
		assert(r.state == State.idle);
	}
	

}
