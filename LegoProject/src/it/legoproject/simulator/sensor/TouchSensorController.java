package it.legoproject.simulator.sensor;

import it.legoproject.Utils;
import it.legoproject.sensor.Sensor;


public class TouchSensorController extends Sensor {

	private final TouchSensor touchSensorL;
	private final TouchSensor touchSensorR;

	public TouchSensorController(final String name, final TouchSensor touchSensorL, final TouchSensor touchSensorR) {
		super(name);
		this.touchSensorL = touchSensorL;
		this.touchSensorR = touchSensorR;
	}

	private String ret = "";
	private String oldSample = "";

	@Override
	public void run() {
		while (true) {
			if (touchSensorL.isPressed()) {
				ret += Utils.L;
			}
			if (touchSensorR.isPressed()) {
				ret += Utils.R;
			}
			if (!ret.equals("") && !ret.equals(oldSample) && obs != null) {
				obs.notify(this, ret + Utils.PRESSED);
				oldSample = ret;
				
//				System.out.println("\n\n\nBOOOOTH PRESSED!!!!!\n\n\n\n");
			}
			ret = "";
			try {
				Thread.sleep(1000);
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
