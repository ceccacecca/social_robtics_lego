package it.legoproject.simulator.sensor;

import it.legoproject.Utils;
import it.legoproject.sensor.Sensor;

public class ColorSensor extends Sensor {

	public ColorSensor(final String name) {
		super(name);

	}

	@Override
	public void run() {
		String oldSample = "";
		int idx = 0;

		while (true) {
			final String tmpSample = Utils.COLORS[(idx++) % Utils.COLORS.length];
			if (!tmpSample.equals(oldSample))
				if (obs != null)
					obs.notify(this, tmpSample);

			try {
				Thread.sleep(10000);
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
