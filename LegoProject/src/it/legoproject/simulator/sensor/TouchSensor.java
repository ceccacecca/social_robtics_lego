package it.legoproject.simulator.sensor;

import it.legoproject.Utils;
import it.legoproject.sensor.Sensor;

import java.util.Random;

public class TouchSensor extends Sensor {

	private Random r;
	public TouchSensor(final String name) {
		super(name);
		r = new Random(); 
	}

	@Override
	public void run() {
		boolean oldSample = false;
		
		
		while (true) {
			final boolean tmpSample = isPressed();
			if (tmpSample != oldSample) {
				if (obs != null && tmpSample) {
					obs.notify(this, name);
				}
			}
			oldSample = tmpSample;

			try {
				Thread.sleep(Utils.SLEEP_TIME);
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean isPressed() {
		int p = r.nextInt(100);
//		System.out.println("Simulating " + name + " " + p);
		if (p == 1) {
			return true;
		} else {
			return false;
		}
	}
}
