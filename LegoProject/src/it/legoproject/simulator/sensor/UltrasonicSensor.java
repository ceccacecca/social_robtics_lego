package it.legoproject.simulator.sensor;

import it.legoproject.Utils;
import it.legoproject.sensor.Sensor;

import java.util.Random;

public class UltrasonicSensor extends Sensor {

	public UltrasonicSensor(final String name) {
		super(name);
	}

	@Override
	public void run() {
		Random r = new Random();
		boolean oldSample = false;

		while (true) {
			float d = r.nextInt(100) / 100f;
			final boolean tmpSample =  d < Utils.DISTANCE_THR ? true: false;
			if (tmpSample != oldSample) {
				if (obs != null && tmpSample) {
					obs.notify(this, d);
				}
				oldSample = tmpSample;
			}

			try {
				Thread.sleep(Utils.SLEEP_TIME);
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
