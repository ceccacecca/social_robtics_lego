package it.legoproject.simulator;

import it.legoproject.IObservable;
import it.legoproject.IObserver;
import it.legoproject.Utils;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Main(final IObserver obs) {
		super("Sensor console");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		final IObservable fake = new IObservable() {

			@Override
			public void setObserver(IObserver obs) {
				// TODO Auto-generated method stub

			}

			@Override
			public String getSensorName() {
				return "fake";
			}
		};

		JPanel content = new JPanel();
		content.setLayout(new GridLayout(8, 2));

		JButton randomAction1 = new JButton(Utils.ACTION1);
		randomAction1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				obs.notify(fake, Utils.ACTION1);
				
			}
		});
		content.add(randomAction1);

		JButton randomAction2 = new JButton(Utils.ACTION2);
//		randomAction2.addActionListener(e -> obs.notify(fake, Utils.ACTION2));
		randomAction2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				obs.notify(fake, Utils.ACTION2);
				
			}
		});
		content.add(randomAction2);

		JButton randomAction3 = new JButton(Utils.ACTION3);
		randomAction3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				obs.notify(fake, Utils.ACTION3);
				
			}
		});
		content.add(randomAction3);

		JButton randomAction4 = new JButton(Utils.ACTION4);
		randomAction4.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				obs.notify(fake, Utils.ACTION4);
				
			}
		});
		content.add(randomAction4);

		JButton randomAction5 = new JButton(Utils.ACTION5);
		randomAction5.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				obs.notify(fake, Utils.ACTION5);
				
			}
		});
		content.add(randomAction5);

		JButton contactL = new JButton(Utils.L_PRESSED);
		contactL.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				obs.notify(fake, Utils.L_PRESSED);
				
			}
		});
		content.add(contactL);

		JButton contactR = new JButton(Utils.R_PRESSED);
		contactR.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				obs.notify(fake, Utils.R_PRESSED);
				
			}
		});
		content.add(contactR);

		JButton contactRL = new JButton(Utils.LR_PRESSED);
		contactRL.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				obs.notify(fake, Utils.LR_PRESSED);
				
			}
		});
		content.add(contactRL);

		final IObservable color = new IObservable() {

			@Override
			public void setObserver(IObserver obs) {
			}

			@Override
			public String getSensorName() {
				return Utils.COLOR_SENSOR;
			}
		};

		for (final String s : Utils.COLORS) {
			JButton c = new JButton(s);
			c.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					obs.notify(color, s);
					
				}
			});
			content.add(c);
		}

		final IObservable distance = new IObservable() {

			@Override
			public void setObserver(IObserver obs) {
			}

			@Override
			public String getSensorName() {
				return Utils.ULTRASONIC_SENSOR;
			}
		};

		final Random r = new Random();
		JButton d = new JButton(Utils.ULTRASONIC_SENSOR);
		d.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				 obs.notify(distance, r.nextInt(10) * 1.0f);
				
			}
		});
		content.add(d);

		getContentPane().add(content);
		setVisible(true);
		pack();
	}

	public static void main(String[] args) {
		new Main(new FakeRobot());
	}
}
