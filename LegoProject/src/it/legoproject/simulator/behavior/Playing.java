package it.legoproject.simulator.behavior;

import it.legoproject.behavior.Behavior;
import lejos.robotics.RegulatedMotor;

public class Playing extends Behavior {

	public Playing(final RegulatedMotor mleft, final RegulatedMotor mright,
			final RegulatedMotor mhead) {
		super(mleft, mright, mhead);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void log(String s) {
		System.out.println("Playing." + s);
	}
}
