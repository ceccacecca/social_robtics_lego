package it.legoproject.simulator.behavior;

import it.legoproject.behavior.Behavior;
import lejos.robotics.RegulatedMotor;

public class Idle extends Behavior {

	public Idle(final RegulatedMotor mleft, final RegulatedMotor mright,
			final RegulatedMotor mhead) {
		super(mleft, mright, mhead);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void log(String s) {
		System.out.println("Idle." + s);
	}
}
