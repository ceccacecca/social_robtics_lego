package it.legoproject.simulator;

import it.legoproject.IObservable;
import it.legoproject.IObserver;
import it.legoproject.Utils;
import it.legoproject.behavior.Behavior;
import it.legoproject.sensor.RandomAction;
import it.legoproject.simulator.behavior.Cleaning;
import it.legoproject.simulator.behavior.Feeding;
import it.legoproject.simulator.behavior.Idle;
import it.legoproject.simulator.behavior.Playing;
import it.legoproject.simulator.behavior.Sleeping;
import it.legoproject.simulator.sensor.ColorSensor;
import it.legoproject.simulator.sensor.TouchSensor;
import it.legoproject.simulator.sensor.TouchSensorController;
import it.legoproject.simulator.sensor.UltrasonicSensor;

public class FakeRobot implements IObserver {
	private final Behavior sleeping, idle, feeding, cleaning, playing;
	public State state;

	public enum State {
		idle, cleaning, playing, feeding, sleeping
	}

	@Override
	public void notify(final IObservable source, final Object data) {
		final String s = source.getSensorName();

		if (s.equals(Utils.COLOR_SENSOR)) {
			transition(data.toString());
		} else {
			switch (state) {

			case idle:
				idle(data, s);
				break;

			case cleaning:
				cleaning(data, s);
				break;

			case playing:
				playing(data, s);
				break;

			case feeding:
				feeding(data, s);
				break;

			case sleeping:
				sleeping(data, s);
				break;
			}
		}
	}

	// ------ BEHAVIORS
	private void cleaning(final Object data, final String s) {
		genericAction(cleaning, data, s);
	}

	private void idle(final Object data, final String s) {
		genericAction(idle, data, s);
	}

	private void genericAction(final Behavior b, final Object data,
			final String source) {
		// System.out.println(data.toString());
		if (source.equals(Utils.COLOR_SENSOR)) {
			b.color(data.toString());
		} else if (source.equals(Utils.ULTRASONIC_SENSOR)) {
			b.distance((Float) data);
		} else {
			switch (data.toString()) {
			case Utils.ACTION1:
				b.action1();
				break;
			case Utils.ACTION2:
				b.action2();
				break;
			case Utils.ACTION3:
				b.action3();
				break;
			case Utils.ACTION4:
				b.action4();
				break;
			case Utils.ACTION5:
				b.action5();
				break;
			case Utils.LR_PRESSED:
				b.lrpressed();
				break;
			case Utils.R_PRESSED:
				b.rpressed();
				break;
			case Utils.L_PRESSED:
				b.lpressed();
				break;
			default:
				throw new IllegalArgumentException();
			}

		}
	}

	private void sleeping(final Object data, final String s) {
		genericAction(sleeping, data, s);
	}

	private void feeding(final Object data, final String s) {
		genericAction(feeding, data, s);
	}

	private void playing(final Object data, final String s) {
		genericAction(playing, data, s);
	}

	// ------ END BEHAVIORS

	public FakeRobot() {
		state = State.idle;
		sleeping = new Sleeping(null, null, null);
		idle = new Idle(null, null, null);
		feeding = new Feeding(null, null, null);
		cleaning = new Cleaning(null, null, null);
		playing = new Playing(null, null, null);

		final RandomAction randomAction = new RandomAction(Utils.RANDOM_ACTION);
		randomAction.setObserver(this);
		randomAction.start();

		final ColorSensor colorSensor = new ColorSensor(Utils.COLOR_SENSOR);
		colorSensor.setObserver(this);
		colorSensor.start();

		final TouchSensor leftTouchSensor = new TouchSensor(Utils.L_PRESSED);
		// leftTouchSensor.setObserver(this);
		// leftTouchSensor.start();

		final TouchSensor rightTouchSensor = new TouchSensor(Utils.R_PRESSED);
		// rightTouchSensor.setObserver(this);
		// rightTouchSensor.start();

		final TouchSensorController touchSensorController = new TouchSensorController(
				Utils.TOUCH_SENSOR_CONTROLLER, leftTouchSensor,
				rightTouchSensor);
		touchSensorController.setObserver(this);
		touchSensorController.start();

		final UltrasonicSensor ultrasonicSensor = new UltrasonicSensor(
				Utils.ULTRASONIC_SENSOR);
		ultrasonicSensor.setObserver(this);
		ultrasonicSensor.start();
	}

	/**
	 * Transition from states given the color
	 *
	 * @param color
	 */
	private void transition(final String color) {
		System.out.println("old state: " + state);
		switch (color) {
		case Utils.BLACK:
			state = State.sleeping;
			break;
		case Utils.WHITE:
			state = State.idle;
			break;
		case Utils.RED:
			state = State.cleaning;
			break;
		case Utils.BLUE:
			state = State.feeding;
			break;
		case Utils.YELLOW:
			state = State.playing;
			break;
		}
		System.out.println("new state: " + state);
	}
}
