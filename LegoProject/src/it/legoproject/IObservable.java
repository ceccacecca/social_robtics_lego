package it.legoproject;

public interface IObservable {
	String getSensorName();

	void setObserver(IObserver obs);
}
