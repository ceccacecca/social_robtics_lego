package it.legoproject;

import it.legoproject.behavior.Behavior;
import it.legoproject.behavior.Cleaning;
import it.legoproject.behavior.Feeding;
import it.legoproject.behavior.Idle;
import it.legoproject.behavior.Playing;
import it.legoproject.behavior.Sleeping;
import it.legoproject.sensor.ButtonSensor;
import it.legoproject.sensor.ColorSensor;
import it.legoproject.sensor.RandomAction;
import it.legoproject.sensor.TouchSensor;
import it.legoproject.sensor.TouchSensorController;
import it.legoproject.sensor.UltrasonicSensor;
import lejos.hardware.Sound;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.robotics.RegulatedMotor;

public class Main implements IObserver {

	final private Behavior sleeping, idle, feeding, cleaning, playing;
	private Behavior current;

	@Override
	public void notify(final IObservable source, final Object data) {
		final String s = source.getSensorName();

		if (s.equals(Utils.COLOR_SENSOR)) {
			transition(data.toString());
		} else {
			switch (current.toString()) {

			case "idle":
				idle(data, s);
				break;

			case "cleaning":
				cleaning(data, s);
				break;

			case "playing":
				playing(data, s);
				break;

			case "feeding":
				feeding(data, s);
				break;

			case "sleeping":
				sleeping(data, s);
				break;
			}
		}
	}

	// ------ BEHAVIORS
	private void cleaning(final Object data, final String s) {
		cleaning.begin();
		genericAction(cleaning, data, s);
	}

	private void idle(final Object data, final String s) {
		genericAction(idle, data, s);
	}

	private synchronized void genericAction(final Behavior b,
			final Object data, final String source) {
		if (source.equals(Utils.COLOR_SENSOR)) {
			b.color(data.toString());
		} else if (source.equals(Utils.ULTRASONIC_SENSOR)) {
			b.distance((Float) data);
		} else {
			switch (data.toString()) {
			case Utils.ACTION1:
				b.action1();
				break;
			case Utils.ACTION2:
				b.action2();
				break;
			case Utils.ACTION3:
				b.action3();
				break;
			case Utils.ACTION4:
				b.action4();
				break;
			case Utils.ACTION5:
				b.action5();
				break;
			case Utils.LR_PRESSED:
				b.lrpressed();
				break;
			case Utils.R_PRESSED:
				b.rpressed();
				break;
			case Utils.L_PRESSED:
				b.lpressed();
				break;
			}
		}
	}

	private void sleeping(final Object data, final String s) {
		genericAction(sleeping, data, s);
	}

	private void feeding(final Object data, final String s) {
		genericAction(feeding, data, s);
	}

	private void playing(final Object data, final String s) {
		genericAction(playing, data, s);
	}

	// ------ END BEHAVIORS

	public Main() {

		Sound.setVolume(100);
		System.out.println("started");
		
		// sensors
		final TouchSensor touchSensorL = new TouchSensor(Utils.TOUCH_SENSOR_L);
		final TouchSensor touchSensorR = new TouchSensor(Utils.TOUCH_SENSOR_R,
				LocalEV3.get().getPort(Utils.PORT_CONTACT_R));
		final TouchSensorController touchSensorController = new TouchSensorController(
				Utils.TOUCH_SENSOR_CONTROLLER, touchSensorL, touchSensorR);
		touchSensorController.setObserver(this);
		final UltrasonicSensor ultrasonicSensor = new UltrasonicSensor(
				Utils.ULTRASONIC_SENSOR);
		ultrasonicSensor.setObserver(this);
		ColorSensor c = new ColorSensor(Utils.COLOR_SENSOR);
		c.setObserver(this);
		ButtonSensor b = new ButtonSensor(Utils.COLOR_SENSOR);
		b.setObserver(this);
		
		// motors
		final RegulatedMotor motorLeft = new EV3LargeRegulatedMotor(
				Utils.MOTOR_LEFT_PORT);
		motorLeft.stop(true);
		final RegulatedMotor motorRight = new EV3LargeRegulatedMotor(
				Utils.MOTOR_RIGHT_PORT);
		motorRight.stop(true);

		final RegulatedMotor motorHead = new EV3MediumRegulatedMotor(
				Utils.MOTOR_HEAD_PORT);
		motorHead.stop(true);

		sleeping = new Sleeping(motorLeft, motorRight, motorHead);
		idle = new Idle(motorLeft, motorRight, motorHead);
		feeding = new Feeding(motorLeft, motorRight, motorHead);
		cleaning = new Cleaning(motorLeft, motorRight, motorHead);
		playing = new Playing(motorLeft, motorRight, motorHead);
		
		current = idle;
//		current.begin();
		
		final RandomAction randomAction = new RandomAction(Utils.RANDOM_ACTION);
		randomAction.setObserver(this);
//		c.start();
		b.start();
		ultrasonicSensor.start();
		touchSensorController.start();
		randomAction.start();

	}

	/**
	 * Transition from states given the color
	 *
	 * @param color
	 */
	private void transition(final String color) {
//		System.out.println("\n\n\n" + color + "\n\n\n");
		Behavior newstate = null;
		switch (color) {
		case Utils.BLACK:
			newstate = sleeping;
			break;
		case Utils.WHITE:
			newstate = idle;
			break;
		case Utils.RED:
			newstate = cleaning;
			break;
		case Utils.BLUE:
			newstate = feeding;
			break;
		case Utils.YELLOW:
			newstate = playing;
			break;
		}
		
		if (newstate != current) {
			current.ends();
			current = newstate;
			current.begin();
			
			System.out.println("\n\n\n" + current.toString() + "\n\n\n");
		}
	}

	public static void main(String[] args) {
		new Main();
	}
}
