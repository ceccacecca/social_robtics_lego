package it.legoproject;

public interface IObserver {
	void notify(IObservable source, Object data);
}
