package it.legoproject;

import lejos.hardware.port.MotorPort;
import lejos.hardware.port.Port;

public class Utils {
	public static final int EAT_THRESHOLD = 5; // robots hungry threshold
	public static final int HUNGRY_MS = 30000;//1000 * 60 * 2;
	public static final int SLEEP_TIME = 00;
	public static final String ACTION1 = "action1";
	public static final String ACTION2 = "action2";
	public static final String ACTION3 = "action3";
	public static final String ACTION4 = "action4";
	public static final String ACTION5 = "action5";
	public static final String COLOR_SENSOR = "colorSensor";
	public static final String YELLOW = "yellow";
	public static final String BLACK = "black";
	public static final String WHITE = "white";
	public static final String GREEN = "green";
	public static final String VIOLET = "violet";
	public static final String BLUE = "blue";
	public static final String RED = "red";
	public static final String COLOR = "color";
	public static final String[] COLORS = { WHITE, YELLOW, BLACK, RED, BLUE };
	public static final String ULTRASONIC_SENSOR = "ultrasonicSensor";
	public static final String TOUCH_SENSOR_CONTROLLER = "touchSensorController";
	public static final String TOUCH_SENSOR_L = "touchSensorL";
	public static final String TOUCH_SENSOR_R = "touchSensorR";
	public static final String RANDOM_ACTION = "randomAction";
	public static final String L = "L";
	public static final String R = "R";
	public static final String PRESSED = "Pressed";
	public static final String LR_PRESSED = L + R + PRESSED;
	public static final String R_PRESSED = R + PRESSED;
	public static final String L_PRESSED = L + PRESSED;
	public static final String PORT_COLOR = "S1";
	public static final String PORT_CONTACT_L = "S3"; // view from the front of the robot
	public static final String PORT_CONTACT_R = "S4"; // view from the front of the robot
	public static final String PORT_ULTRASONIC = "S2";
	public static final long RAND_TIME = 6000;
	public static final int ACTION_NUMBER = 5;
	public static final String ACTION = "action";
	public static final Float DISTANCE_THR = 0.10f;
	public static final Port MOTOR_LEFT_PORT = MotorPort.A;  // view from the front of the robot
	public static final Port MOTOR_RIGHT_PORT = MotorPort.D;  // view from the front of the robot
	public static final Port MOTOR_HEAD_PORT = MotorPort.C;  
}
