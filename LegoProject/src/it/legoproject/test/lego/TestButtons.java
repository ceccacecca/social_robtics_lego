package it.legoproject.test.lego;

import it.legoproject.IObservable;
import it.legoproject.IObserver;
import it.legoproject.Utils;
import it.legoproject.behavior.Behavior;
import it.legoproject.behavior.Idle;
import it.legoproject.behavior.Sleeping;
import it.legoproject.sensor.RandomAction;
import it.legoproject.sensor.TouchSensor;
import it.legoproject.sensor.TouchSensorController;
import it.legoproject.sensor.UltrasonicSensor;
import lejos.hardware.Sound;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.robotics.RegulatedMotor;

public class TestButtons implements IObserver {

	final private Behavior playing;

	@Override
	public void notify(final IObservable source, final Object data) {
		final String s = source.getSensorName();
		playing(data, s);

	}

	private void genericAction(final Behavior b, final Object data,
			final String source) {
		if (source.equals(Utils.COLOR_SENSOR)) {
			b.color(data.toString());
		} else if (source.equals(Utils.ULTRASONIC_SENSOR)) {
			b.distance((Float) data);
		} else {
			switch (data.toString()) {
			case Utils.ACTION1:
				b.action1();
				break;
			case Utils.ACTION2:
				b.action2();
				break;
			case Utils.ACTION3:
				b.action3();
				break;
			case Utils.ACTION4:
				b.action4();
				break;
			case Utils.ACTION5:
				b.action5();
				break;
			case Utils.LR_PRESSED:
				b.lrpressed();
				break;
			case Utils.R_PRESSED:
				b.rpressed();
				break;
			case Utils.L_PRESSED:
				b.lpressed();
				break;
			}
		}
	}

	private void playing(final Object data, final String s) {
		genericAction(playing, data, s);
	}

	// ------ END BEHAVIORS

	public TestButtons() {
		System.out.println("started");
		Sound.setVolume(100);

		// sensors
		final TouchSensor touchSensorL = new TouchSensor(Utils.TOUCH_SENSOR_L);
		System.out.println("left ok");
		final TouchSensor touchSensorR = new TouchSensor(Utils.TOUCH_SENSOR_R,
				LocalEV3.get().getPort(Utils.PORT_CONTACT_R));
		System.out.println("right ok");
		final TouchSensorController touchSensorController = new TouchSensorController(
				Utils.TOUCH_SENSOR_CONTROLLER, touchSensorL, touchSensorR);
		touchSensorController.setObserver(this);

		final UltrasonicSensor ultrasonicSensor = new UltrasonicSensor(
				Utils.ULTRASONIC_SENSOR);
		ultrasonicSensor.setObserver(this);

		// motors
		final RegulatedMotor motorLeft = new EV3LargeRegulatedMotor(
				Utils.MOTOR_LEFT_PORT);
		motorLeft.stop(true);
		final RegulatedMotor motorRight = new EV3LargeRegulatedMotor(
				Utils.MOTOR_RIGHT_PORT);
		motorRight.stop(true);

		final RegulatedMotor motorHead = new EV3MediumRegulatedMotor(
				Utils.MOTOR_HEAD_PORT);
		motorHead.stop(true);

		// final RandomAction randomAction = new
		// RandomAction(Utils.RANDOM_ACTION,1);
		// playing = new Playing(motorLeft, motorRight, motorHead);

		// playing = new Feeding(motorLeft, motorRight, motorHead);

		// playing = new Cleaning(motorLeft, motorRight, motorHead);
		// final RandomAction randomAction = new
		// RandomAction(Utils.RANDOM_ACTION, 5);

		// final RandomAction randomAction = new
		// RandomAction(Utils.RANDOM_ACTION,5);
		// playing = new Idle(motorLeft, motorRight, motorHead);

		final RandomAction randomAction = new RandomAction(Utils.RANDOM_ACTION,
				5);
		playing = new Sleeping(motorLeft, motorRight, motorHead);
		randomAction.setObserver(this);

		touchSensorController.start();
		randomAction.start();
		ultrasonicSensor.start();

	}

	public static void main(String[] args) {
		new TestButtons();
	}
}
