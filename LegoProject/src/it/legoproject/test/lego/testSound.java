package it.legoproject.test.lego;

import java.io.File;

import lejos.hardware.Sound;

public class testSound {
	public static void main(String[] args) {
//		System.out.println(""+Sound.playSample(new File("Programs/MONO_Whistling Person_1.wav")));
//		System.out.println(""+Sound.playSample(new File("MONO_Whistling Person_1.wav")));
		
//		System.out.println(""+Sound.playSample(new File("whistle1.wav")));
		
//		System.out.println("w2: " + Sound.playSample(new File("Programs/whistle2.wav")));
//		try {
//			Thread.sleep(2000);
//		} catch (final InterruptedException e) {
//			e.printStackTrace();
//		}
//		System.out.println("w2: ");
//		Sound.playSample(new File("whistle2.wav"));
//		System.out.println("w2: ");
//		try {
//			Thread.sleep(4000);
//		} catch (final InterruptedException e) {
//			e.printStackTrace();
//		}
//		Sound.beep();
//		System.exit(0);
		
		soundEating();
		
	}
	
	public static void soundBothered ()	{
		boolean out = false;
		for (int i = 0; i < 2; i++)	{
			Sound.playTone(400,50);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (i == 1 && !out)	{
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				out = true;
				i=-1;
			}
				
		}
	}
	
	public static void soundIdle1 ()	{
		boolean reverse = false;
		for (int i = 0; i < 20; i++)	{
			if (!reverse){
				Sound.playTone(700+i*5,50); 
			}
			else{
				Sound.playTone(800-i*5,50);
			}
			if (i == 19 && !reverse)	{
				i=0;
				reverse = true;
			}
		}
	}
	
	public static void soundIdle2	 ()	{
		boolean out = false;
		boolean second = false;
		for (int i = 0; i < 3; i++)	{
			if (!second)
				Sound.playTone(500+i*100,100);
			else
				Sound.playTone(700+i*100,100);
			if (i == 2)	{
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (!out)
					i=-1;
				out = true;
				second = true;
			}
		}
	}
	
	public static void soundNoMoreFood ()	{
		Sound.playTone(650,800);
	}
	
	public static void soundEating	 ()	{
		Sound.playTone(500,200);
		Sound.playTone(700,200);
	}	
}
