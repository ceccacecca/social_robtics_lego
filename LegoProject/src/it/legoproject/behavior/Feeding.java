package it.legoproject.behavior;

import java.io.File;
import java.util.Random;

import it.legoproject.SoundUtils;
import it.legoproject.Utils;
import lejos.hardware.Sound;
import lejos.robotics.RegulatedMotor;

public class Feeding extends Behavior {
	private int count;
	private long startfeeding;

	public Feeding(final RegulatedMotor mleft, final RegulatedMotor mright,
			final RegulatedMotor mhead) {
		super(mleft, mright, mhead);
		// TODO Auto-generated constructor stub
		count = 0;
	}

	@Override
	protected void log(String s) {
		System.out.println("Feeding." + s);
	}

	@Override
	public void distance(Float distance) {
		super.distance(distance);
		// follow the feeder
		mleft.rotate(-90, true);
		mright.rotate(-90, true);
	}

	@Override
	public void lpressed() {
		super.lpressed();
		eat();
	}

	@Override
	public void rpressed() {
		super.rpressed();
		eat();
	}

	@Override
	public void lrpressed() {
		super.lrpressed();
		eat();
	}

	private void eat() {
		if (count == Utils.EAT_THRESHOLD)
			if (System.currentTimeMillis() - startfeeding <= Utils.HUNGRY_MS) {
				//Sound.twoBeeps(); // TODO play "I'm not hungry sound!"
				SoundUtils.soundNoMoreFood();
				return;
			} else {
				count = 0;
			}

		if (count == 0)
			startfeeding = System.currentTimeMillis();

		count++;
//		Random r = new Random();
//		int randAction = r.nextInt(1);
//		if (randAction == 0)
//			System.out.println(""+Sound.playSample(new File("eating1.wav")));
//		else
//			System.out.println(""+Sound.playSample(new File("eating2.wav")));
//		Sound.beep();

		SoundUtils.soundEating();
		try {
			// Do not take other inputs (freeze the robot)
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String toString() {
		return "feeding";
	}
}
