package it.legoproject.behavior;

import java.io.File;
import java.util.Random;

import lejos.hardware.Sound;
import lejos.robotics.RegulatedMotor;

public class Playing extends Behavior {

	public Playing(final RegulatedMotor mleft, final RegulatedMotor mright,
			final RegulatedMotor mhead) {
		super(mleft, mright, mhead);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void log(String s) {
		System.out.println("Playing." + s);
	}
	
	@Override
	public void distance(Float distance) {
		super.distance(distance);
		// RUN AWAY!!
		mleft.rotate(90, true);
		mright.rotate(90, true);
	}
	
	@Override
	public  void rpressed() {
		super.rpressed();
		mright.stop();
		mleft.rotate(-90, true);
	}
	
	@Override
	public  void lpressed() {
		super.lpressed();
		mleft.stop();
		mright.rotate(-90, true);
	}
	
	@Override
	public  void lrpressed() {
		super.lrpressed();
		mright.rotate(-90, true);
		mleft.rotate(-90, true);
	}
	
	@Override
	public  void action1() {
		super.action1();
//		mhead.set\
		mhead.setSpeed((int)mhead.getMaxSpeed());
		mhead.rotate(720,true);
//		mleft.rotate(-90, true);
//		Random r = new Random();
//		int randAction = r.nextInt(1);
//		if (randAction == 0)
//			System.out.println(""+Sound.playSample(new File("play1.wav")));
//		else
//			System.out.println(""+Sound.playSample(new File("play2.wav")));
		Sound.twoBeeps();
//		LocalEV3.get().getAudio().playSample(new File("sounds/snoring.wav"));
	}
	
	@Override
	public String toString() {
		return "playing";
	}
}
