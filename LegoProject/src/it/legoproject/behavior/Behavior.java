package it.legoproject.behavior;

import lejos.robotics.RegulatedMotor;

public abstract class Behavior {
	final protected RegulatedMotor mleft;
	final protected RegulatedMotor mright;
	final protected RegulatedMotor mhead;
	private boolean beginned;

	public Boolean isBeginned() {
		return beginned;
	}

	public Behavior(final RegulatedMotor mleft, final RegulatedMotor mright,
			final RegulatedMotor mhead) {
		this.mleft = mleft;
		this.mright = mright;
		this.mhead = mhead;
		this.beginned = false;
	}

	/**
	 * What to do when the behavior begins
	 */
	public void begin() {
		if (!isBeginned()) {
			log("begin");
			beginned = true;
			tobegin();
		}
	}

	protected void tobegin() {
	}

	/**
	 * What to do when the behavior ends
	 */
	public void ends() {
		log("end");
	}

	/**
	 * Generic action
	 */
	public void action1() {
		log("action1");
	}

	/**
	 * Generic action
	 */
	public void action2() {
		log("action2");
	}

	/**
	 * Generic action
	 */
	public void action3() {
		log("action3");
	}

	/**
	 * Generic action
	 */
	public void action4() {
		log("action4");
	}

	/**
	 * Generic action
	 */
	public void action5() {
		log("action5");
	}

	/**
	 * Reaction to left-contact sensor
	 */
	public void lpressed() {
		log("lpressed");
	}

	/**
	 * Reaction to right-contact sensor
	 */
	public void rpressed() {
		log("rpressed");
	}

	/**
	 * Reaction to both-contact sensor
	 */
	public void lrpressed() {
		log("lrpressed");
	}

	/**
	 * Reaction to perceived color
	 */
	public void color(String color) {
		log("color: " + color);
	}

	/**
	 * Reaction to the perceived distance
	 */
	public void distance(Float distance) {
		log("distance: " + distance);
	}

	protected void log(String s) {
		System.out.println(s);
	}

	/**
	 * Stop the wheels
	 */
	public void stop() {
		mright.stop();
		mleft.stop();
	}

	/**
	 * Stop the head
	 */
	public void headstop() {
		mhead.stop();
	}
}
