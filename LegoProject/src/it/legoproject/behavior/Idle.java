package it.legoproject.behavior;

import it.legoproject.SoundUtils;

import java.io.File;

import lejos.hardware.Sound;
import lejos.robotics.RegulatedMotor;

public class Idle extends Behavior {

	public Idle(final RegulatedMotor mleft, final RegulatedMotor mright,
			final RegulatedMotor mhead) {
		super(mleft, mright, mhead);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void log(String s) {
		System.out.println("Idle." + s);
	}
	
	@Override
	public void action1() {
		super.action1();
		System.out.println("whistle1.");
		SoundUtils.soundIdle1();
//		System.out.println(""+Sound.playSample(new File("whistle1.wav")));
	}
	
	@Override
	public void action2() {
		super.action2();
		System.out.println("whistle2.");
//		System.out.println(""+Sound.playSample(new File("whistle2.wav")));
		SoundUtils.soundIdle2();
	}
	
	@Override
	public void action3() {
		super.action3();
		// TODO bored
	}
	
	@Override
	public void action4() {
		super.action4();
		mhead.rotate(1080, true);
	}
	
	@Override
	public void action5() {
		super.action5();
		mleft.rotate(-720, true);
		mright.rotate(720, true);
	}
	
	@Override
	public String toString() {
		return "idle";
	}
}
