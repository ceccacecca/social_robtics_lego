package it.legoproject.behavior;

import it.legoproject.SoundUtils;

import java.io.File;

import lejos.hardware.Sound;
import lejos.robotics.RegulatedMotor;

public class Sleeping extends Behavior {

	public Sleeping(final RegulatedMotor mleft, final RegulatedMotor mright,
			final RegulatedMotor mhead) {
		super(mleft, mright, mhead);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void log(String s) {
		System.out.println("Sleeping." + s);
	}

	@Override
	public void ends() {
		super.ends();
		mhead.stop();
	}
	
	@Override
	public void action1() {
		super.action1();
//		System.out.println(""+Sound.playSample(new File("snore1.wav")));
		Sound.beep();
	}

	@Override
	public void action2() {
		super.action2();
//		System.out.println(""+Sound.playSample(new File("snore2.wav")));
		Sound.twoBeeps();
	}
	
	@Override
	public void lpressed() {
		super.lpressed();
		bothered();
		mleft.rotate(720, true);
		mright.rotate(-720, true);
	}
	
	@Override
	public void rpressed() {
		super.lpressed();
		bothered();
		mleft.rotate(-720, true);
		mright.rotate(720, true);
	}
	
	@Override
	public void lrpressed() {
		super.lrpressed();
		bothered();
		mleft.rotate(720, true);
		mright.rotate(720, true);
	}
	@Override
	public String toString() {
		return "sleeping";
	}
	private void bothered()	{
		SoundUtils.soundBothered();
	}
}
