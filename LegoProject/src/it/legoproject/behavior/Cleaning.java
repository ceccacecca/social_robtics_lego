package it.legoproject.behavior;

import java.io.File;

import lejos.hardware.Sound;
import lejos.robotics.RegulatedMotor;

public class Cleaning extends Behavior {
	private boolean forward;

	public Cleaning(final RegulatedMotor mleft, final RegulatedMotor mright,
			final RegulatedMotor mhead) {
		super(mleft, mright, mhead);
		// TODO Auto-generated constructor stub
		forward = true;
	}

	@Override
	public void tobegin() {
		super.tobegin();
//		mhead.setSpeed((int) mhead.getMaxSpeed() / 2);
		mhead.forward();
	}

	@Override
	public void ends() {
		super.ends();
		mhead.stop();
	}

	@Override
	public void action1() {
		super.action1();
		Sound.beep();
//		System.out.println(""+Sound.playSample(new File("cleaning1.wav")));
	}

	@Override
	public void action2() {
		super.action2();
//		System.out.println(""+Sound.playSample(new File("cleaning2.wav")));
		Sound.twoBeeps();
	}

	@Override
	public void action3() {
		super.action3();
		mleft.rotate(-360);
		mright.rotate(360);
		mleft.rotate(360);
	}

	@Override
	public void action4() {
		super.action4();

		mright.rotate(-360);
		mleft.rotate(360);
		mright.rotate(360);

	}

	@Override
	public void action5() {
		super.action5();
		mhead.stop();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		mhead.setSpeed((int)mhead.getMaxSpeed());
		if (forward)
			mhead.backward();
		else
			mhead.forward();

		forward = !forward;
	}
	
	@Override
	public void lpressed() {
		super.lpressed();
		
		// TODO song sound1?
	}
	
	@Override
	public void rpressed() {
		super.rpressed();
		
		// TODO song sound2?
	}

	@Override
	protected void log(String s) {
		System.out.println("Cleaning." + s);
	}

	@Override
	public String toString() {
		return "cleaning";
	}
}
