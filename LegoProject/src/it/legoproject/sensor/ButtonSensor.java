package it.legoproject.sensor;

import it.legoproject.Utils;
import lejos.hardware.Button;

public class ButtonSensor extends Sensor {

	public ButtonSensor(final String name) {
		super(name);
	}

	private int prev = -1;

	@Override
	public void run() {

		while (true) {
			int cur = Button.waitForAnyPress();

			if (obs != null && cur != prev) {
				switch (cur) {
				case Button.ID_DOWN:
					obs.notify(this, Utils.RED);
					break;
				// case 0: obs.notify(this, Color.GREEN); break;
				case Button.ID_LEFT:
					obs.notify(this, Utils.BLUE);
					break;
				case Button.ID_RIGHT:
					obs.notify(this, Utils.YELLOW);
					break;
				case Button.ID_ENTER:
					obs.notify(this, Utils.WHITE);
					break;
				case Button.ID_UP:
					obs.notify(this, Utils.BLACK);
					break;
				}

			}
			prev = cur;
		}

	}

}
