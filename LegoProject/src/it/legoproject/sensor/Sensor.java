package it.legoproject.sensor;

import it.legoproject.IObservable;
import it.legoproject.IObserver;
import lejos.hardware.port.Port;

public class Sensor extends Thread implements IObservable {

	protected final String name;
	protected IObserver obs;
	private Port port;

	public Sensor(final String name) {
		this.name = name;
	}

	@Override
	public String getSensorName() {
		return name;
	}

	@Override
	public void setObserver(final IObserver obs) {
		this.obs = obs;
	}

	public void setPort(final Port port) {
		this.port = port;
	}

	public Port getPort() {
		return port;
	}
}
