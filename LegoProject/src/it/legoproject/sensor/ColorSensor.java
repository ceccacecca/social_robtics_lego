package it.legoproject.sensor;

import it.legoproject.Utils;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.SensorModes;
import lejos.robotics.SampleProvider;

public class ColorSensor extends Sensor {

	private final SensorModes colorSensor;

	public ColorSensor(final String name) {
		super(name);

		setPort(LocalEV3.get().getPort(Utils.PORT_COLOR));
		colorSensor = new EV3ColorSensor(getPort());
	}

	@Override
	public void run() {
		// get an instance of the ultrasonic sensor in the measurement mode
		final SampleProvider colorRGB = colorSensor.getMode("ColorID");

		// Inizializza un array di float per caricare i samples
		// SampleProvider mi da la lunghezza dell'array
		final float[] colorRGBSample = new float[colorRGB.sampleSize()];
		float oldSample = -1;

		// public static final int RED = 0;
		// public static final int GREEN = 1;
		// public static final int BLUE = 2;
		// public static final int YELLOW = 3;
		// public static final int MAGENTA = 4;
		// public static final int ORANGE = 5;
		// public static final int WHITE = 6;
		// public static final int BLACK = 7;
		// public static final int PINK = 8;
		// public static final int GRAY = 9;
		// public static final int LIGHT_GRAY = 10;
		// public static final int DARK_GRAY = 11;
		// public static final int CYAN = 12;
		// public static final int BROWN = 13;
		// public static final int NONE = -1;

		while (true) {
			colorRGB.fetchSample(colorRGBSample, 0);
			final float tmpSample = colorRGBSample[0];
			if (tmpSample != oldSample) {
				if (obs != null) {
					switch ((int) tmpSample) {
					case 0:
						obs.notify(this, Utils.RED);
						break;
					// case 0: obs.notify(this, Color.GREEN); break;
					case 1:
						obs.notify(this, Utils.BLUE);
						break;
					case 3:
						obs.notify(this, Utils.YELLOW);
						break;
					case 2:
						obs.notify(this, Utils.WHITE);
						break;
					case 7:
						obs.notify(this, Utils.BLACK);
						break;
					}

				}
			}

			oldSample = tmpSample;

			try {
				Thread.sleep(Utils.SLEEP_TIME);
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
