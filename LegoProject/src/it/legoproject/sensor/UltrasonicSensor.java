package it.legoproject.sensor;

import it.legoproject.Utils;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.hardware.sensor.SensorModes;
import lejos.robotics.SampleProvider;

public class UltrasonicSensor extends Sensor {

	private final SensorModes ultrasonicSensor;

	public UltrasonicSensor(final String name) {
		super(name);
		setPort(LocalEV3.get().getPort(Utils.PORT_ULTRASONIC));
		ultrasonicSensor = new EV3UltrasonicSensor(getPort());
	}

	@Override
	public void run() {
		// get an instance of the ultrasonic sensor in the measurement mode
		final SampleProvider distance = ultrasonicSensor.getMode("Distance");

		// Inizializza un array di float per caricare i samples
		// SampleProvider mi da la lunghezza dell'array
		final float[] distanceSample = new float[distance.sampleSize()];
		boolean oldSample = false;

		while (true) {
			distance.fetchSample(distanceSample, 0);
			float d = distanceSample[0];
			final boolean tmpSample = d < Utils.DISTANCE_THR ? true : false;
			//if (tmpSample != oldSample) {
				if (obs != null && tmpSample) {
					obs.notify(this, d);
				}
				oldSample = tmpSample;
			//}

			try {
				Thread.sleep(Utils.SLEEP_TIME);
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
