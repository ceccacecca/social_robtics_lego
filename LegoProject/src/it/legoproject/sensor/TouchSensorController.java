package it.legoproject.sensor;

import it.legoproject.Utils;


public class TouchSensorController extends Sensor {

	private final TouchSensor touchSensorL;
	private final TouchSensor touchSensorR;

	public TouchSensorController(final String name, final TouchSensor touchSensorL, final TouchSensor touchSensorR) {
		super(name);
		this.touchSensorL = touchSensorL;
		this.touchSensorR = touchSensorR;
	}

	private String ret = "";
	private String oldSample = "";

	@Override
	public void run() {
		while (true) {
			if (touchSensorL.isPressed()) {
				ret += Utils.L;
			}
			if (touchSensorR.isPressed()) {
				ret += Utils.R;
			}
			if (!ret.equals("") /*&& !ret.equals(oldSample)*/ && obs != null) {
				obs.notify(this, ret + Utils.PRESSED);
				oldSample = ret;
			}
			ret = "";
			try {
				Thread.sleep(Utils.SLEEP_TIME);
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
