package it.legoproject.sensor;

import it.legoproject.Utils;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.SensorModes;
import lejos.robotics.SampleProvider;

public class TouchSensor extends Sensor {

	private final SensorModes touchSensor;
	// get an instance of the ultrasonic sensor in the measurement mode
	private final SampleProvider touch;
	// Inizializza un array di float per caricare i samples
	private final float[] touchSample;

	public TouchSensor(final String name) {
		this(name, null);

	}

	public TouchSensor(String name, Port port) {
		super(name);
		if (port == null)
			super.setPort(LocalEV3.get().getPort(Utils.PORT_CONTACT_L));
		else
			super.setPort(port);
		touchSensor = new EV3TouchSensor(super.getPort());
		touch = touchSensor.getMode("Touch");
		touchSample = new float[touch.sampleSize()];
	}

	@Override
	public void run() {
		boolean oldSample = false;

		while (true) {
			final boolean tmpSample = isPressed();
			if (tmpSample != oldSample) {
				if (obs != null && tmpSample) {
					obs.notify(this, name);
				}
				oldSample = tmpSample;
			}

			try {
				Thread.sleep(Utils.SLEEP_TIME);
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean isPressed() {
		touch.fetchSample(touchSample, 0);
		if (touchSample[0] == 1) {
			return true;
		} else {
			return false;
		}
	}
}
