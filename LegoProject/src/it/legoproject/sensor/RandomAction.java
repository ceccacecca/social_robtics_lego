package it.legoproject.sensor;

import it.legoproject.IObservable;
import it.legoproject.IObserver;
import it.legoproject.Utils;

import java.util.Random;

public class RandomAction extends Thread implements IObservable {

	private IObserver obs;
	private final Random r;
	private final String name;
	private final int actionNumber;
	@Override
	public String getSensorName() {
		return name;
	}

	@Override
	public void setObserver(final IObserver obs) {
		this.obs = obs;
	}

	public RandomAction(final String name) {
		this (name, Utils.ACTION_NUMBER);
	}
	
	public RandomAction(final String name, int actionNumber) {
		this.name = name;
		this.r = new Random();
		this.actionNumber = actionNumber;
	}

	@Override
	public void run() {

		int randAction;
		while (true) {
			randAction = r.nextInt(actionNumber);
			obs.notify(this, Utils.ACTION + (randAction + 1));
			try {
				Thread.sleep(Utils.RAND_TIME);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

}
